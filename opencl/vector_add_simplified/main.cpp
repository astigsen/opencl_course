///original author: Erwin Coumans
#include "btOpenCLUtils.h"
#include "../parallel_primitives/host/btOpenCLArray.h"
#include "../parallel_primitives/host/btLauncherCL.h"
#include <stdio.h>
struct float4
{
	float s[4];
};

#define MSTRINGIFY(A) #A
const char* kernelString= MSTRINGIFY(
__kernel void VectorAdd(__global const float4* a, __global const float4* b, __global float4* c, int numElements)
{
  int iGID = get_global_id(0);
	if (iGID>=numElements)
		return;
	float4 aGID = a[iGID];
	float4 bGID = b[iGID];
	float4 result = aGID + bGID;
    c[iGID] = result;
}
);

int main(int argc, char* argv[])
{
	int ciErrNum = 0;
	int preferred_device = -1;
	int preferred_platform = -1;
	cl_platform_id		platformId;
	cl_context			ctx;
	cl_command_queue	queue;
	cl_device_id		device;
	cl_kernel			addKernel;
	ctx = btOpenCLUtils::createContextFromType(CL_DEVICE_TYPE_GPU, &ciErrNum,0,0,preferred_device,preferred_platform,&platformId);
	btOpenCLUtils::printPlatformInfo(platformId);
	oclCHECKERROR(ciErrNum, CL_SUCCESS);
	if (!ctx) {
		printf("No OpenCL capable GPU found!");
		return 0;
	}

	device = btOpenCLUtils::getDevice(ctx,0);
	queue = clCreateCommandQueue(ctx, device, 0, &ciErrNum);
	addKernel = btOpenCLUtils::compileCLKernelFromString(ctx,device,kernelString,"VectorAdd",&ciErrNum);
	oclCHECKERROR(ciErrNum, CL_SUCCESS);
	int numElements = 32;
	btOpenCLArray<float4> a(ctx,queue); 
	btOpenCLArray<float4> b(ctx,queue); 
	btOpenCLArray<float4> c(ctx,queue);
	for (int i=0;i<numElements;i++)
	{
		float4 v = {1,2,3,4};
		a.push_back(v,false);
		b.push_back(v,false);
	}
	c.resize(numElements);
	btBufferInfoCL bInfo[] = { btBufferInfoCL( a.getBufferCL() ), btBufferInfoCL( b.getBufferCL() ), btBufferInfoCL( c.getBufferCL() ) };
	btLauncherCL launcher( queue, addKernel);
	launcher.setBuffers( bInfo, sizeof(bInfo)/sizeof(btBufferInfoCL) );
	launcher.setConst(  numElements );
	launcher.launch1D( numElements);
	for (int i=0;i<numElements;i++)
	{
		float4 v = c.at(i);
		printf("c[%d]=(%f,%f,%f,%f)\n",i,v.s[0],v.s[1],v.s[2],v.s[3]);
	}
	clReleaseCommandQueue(queue);
	clReleaseContext(ctx);
	return 0;
}