OpenCL course
Erwin Coumans

Introduction
============

This is material and source code for the OpenCL course
http://www.escience.ku.dk/research_school/phd_courses/upcoming/opencl 


Building
========

You can build the project executables under Windows, Mac OSX and Linux.
In all cases the binary executables will be available in the bin folder.

Microsoft Windows Visual Studio 2010 or later
===============================

Click on build/vs2010.bat
Click on build/vs2010/0MySolution.sln


Mac OSX and Xcode
=================

Click on build/xcode4.command and open the XCode project in build/xcode4

Alternatively you can use a terminal window

cd build
./premake_osx xcode4
open xcode4/0MySolution.xcworkspace

Makefile (Linux and Mac OSX)
============================

open a terminal console and use

cd build
./premake_linux gmake
cd gmake
make



Licensing
=========

All the source code and OpenCL kernels are available under the permissive ZLib license

